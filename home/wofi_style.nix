{ colors ? import ./colors.nix }:

''
  window {
    margin: 0px;
    border: 1px solid ${colors.foreground};
    background-color: ${colors.background};
  }

  #img, #label {
    display: none;
  }

  #input {
    margin: 5px;
    border: none;
    color: ${colors.foreground};
    background-color: ${colors.background};
  }


  #inner-box {
    margin: 5px;
    border: none;
    background-color: ${colors.background};
  }

  #outer-box {
    margin: 5px;
    border: none;
    background-color: ${colors.background};
  }

  #scroll {
    margin: 0px;
    border: none;
  }

  #text {
    color: ${colors.foreground};
    margin: 5px;
    border: none;
  } 

  #entry:selected {
    background-color: ${colors.color4};
  }
''
