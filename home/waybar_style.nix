{ colors ? import ./colors.nix }:

''
  * {
    border: none;
    border-radius: 0;
    /* `otf-font-awesome` is required to be installed for icons */
    font-family: Inconsolata, Roboto, Helvetica, Arial, sans-serif;
    font-size: 16px;
    /* min-height: 25px; */
  }
  
  /*
  window#waybar {
      background: ${colors.background};
      color: ${colors.foreground};
      transition-property: background-color;
      transition-duration: .5s;
  } */

  window#waybar {
    background: transparent;
  }

  window > box {
      margin: 25px;
      background: ${colors.background};
      color: ${colors.foreground};
      transition-property: background-color;
      transition-duration: .5s;
  }
  
  /*
  window#waybar.empty {
      background-color: transparent;
  }
  window#waybar.solo {
      background-color: #FFFFFF;
  }
  window#waybar.termite {
      background-color: ${colors.background};
  }
  window#waybar.qutebrowser {
      background-color: ${colors.color4};
  }
  */
  
  #workspaces button {
      padding: 0 5px;
      background-color: transparent;
      color: #ffffff;
      border-bottom: 3px solid transparent;
  }
  
  /* https://github.com/Alexays/Waybar/wiki/FAQ#the-workspace-buttons-have-a-strange-hover-effect */
  #workspaces button:hover {
      background: rgba(0, 0, 0, 0.2);
      box-shadow: inherit;
      border-bottom: 3px solid ${colors.foreground};
  }
  
  #workspaces button.focused {
      background-color: ${colors.color8};
      border-bottom: 3px solid ${colors.foreground};
  }
  
  #workspaces button.urgent {
      background-color: ${colors.color1};
  }
  
  #mode {
      background-color: ${colors.color1};
      border-bottom: 3px solid ${colors.foreground};
  }
  
  #clock,
  #battery,
  #backlight,
  #network,
  #pulseaudio,
  #custom-media,
  #tray,
  #mode,
  #idle_inhibitor,
  #mpd {
      padding: 0 10px;
      margin: 0 4px;
      color: ${colors.foreground};
  }
  
  /* BATTERY */
  #battery {
      background-color: ${colors.color1};
      color: ${colors.background};
  }
  
  #battery.charging {
      background-color: ${colors.color2};
      color: ${colors.background};
  }
  
  @keyframes blink {
      to {
          background-color: ${colors.color1};
          color: ${colors.foreground};
      }
  }
  
  #battery.critical:not(.charging) {
      background-color: ${colors.color1};
      color: ${colors.background};
      animation-name: blink;
      animation-duration: 0.5s;
      animation-timing-function: linear;
      animation-iteration-count: infinite;
      animation-direction: alternate;
  }
  
  /* SOMETHING */
  label:focus {
      background-color: ${colors.color4};
  }
  
  /* NETWORK */
  #network {
      color: ${colors.background};
      background-color: ${colors.color6};
  }
  
  #network.disconnected {
      background-color: ${colors.color1};
  }

  /* AUDIO */
  #pulseaudio.muted {
      background-color: ${colors.color1};
  }
  
  /* PERFORMANCE STATS */
  #cpu,
  #temperature,
  #memory {
      background-color: ${colors.color12};
      padding: 0 10px;
      margin: 0 4px;
      color: ${colors.background};
  }
  #temperature.critical,
  #cpu.critical,
  #memory.critical {
      background-color: ${colors.color1};
  }
  
  #idle_inhibitor.activated {
      background-color: ${colors.color8};
      color: ${colors.foreground};
  }
  
  /* not used
  #custom-media {
      background-color: #66cc99;
      color: #2a5c45;
      min-width: 100px;
  }
  
  #custom-media.custom-spotify {
      background-color: #66cc99;
  }
  
  #tray {
      background-color: #2980b9;
  }

  #custom-media.custom-vlc {
      background-color: #ffa000;
  }
  
  #mpd {
      background-color: #66cc99;
      color: #2a5c45;
  }
  
  #mpd.disconnected {
      background-color: #f53c3c;
  }
  
  #mpd.stopped {
      background-color: #90b1b1;
  }
  
  #mpd.paused {
      background-color: #51a37a;
  }
  
  */
''
