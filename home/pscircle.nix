{ colors ? import ./colors.nix, sway }:

''
  #!/usr/bin/env sh

  # make new bg
  pscircle --output=~/.bg \
    --background-color=${colors.color0} \
     --tree-font-color=${colors.foreground} \
     --dot-color-min=${colors.color4} \
     --dot-color-max=${colors.color1} \
     --dot-border-color-min=${colors.color12} \
     --dot-border-color-max=${colors.color9} \
     --link-color-min=${colors.color14} \
     --link-color-max=${colors.color9} \
     --toplists-font-color=${colors.foreground} \
     --toplists-pid-font-color=${colors.foreground} \
     --toplists-font-face=Sans \
     --toplists-bar-background=${colors.background} \
     --toplists-bar-color=${colors.foreground}

  # notify sway of bg change
  ${sway}/bin/swaymsg output "*" background ~/.bg fill
''
