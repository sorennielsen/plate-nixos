{ pkgs ? import <nixpkgs> { config = { allowUnfree = true; }; } }:

{
  enable = true;
  vimAlias = false;
  viAlias = false;
  plugins = with pkgs.vimPlugins; [
    ale                 # use all the linters!
    syntastic           # another universal linter!
    base16-vim          # pretty colors in vim
    # completion-nvim     # autocompletion
    # completion-tabnine  # ML-based autocomplete # XXX: unfree
    coc-nvim            # completion engine
    coc-tsserver
    tabular             # align text
    ctrlp               # fuzzy file search
    elm-vim             # elm lang stuff
    nerdtree            # pretty file tree
    nim-vim             # nim lang stuff
    rust-vim            # rust lang stuff
    vim-airline         # pretty bar at bottom of window
    vim-airline-themes  # themes for same
    typescript-vim      # typescript syntax higlighting
    tsuquyomi           # typescript toolz
    vim-jsx-typescript  # for ts react stuff
    vim-pandoc          # a good markdown flavor
    colorizer           # highlight color codes
    solidity            # highlighting for smart contracts
    gitgutter           # show symbols for new lines
    vim-lsp
    undotree            # make editing the undo tree easier
    vim-easymotion      # jump labels in vim
    vim-bookmarks       # 🔖
    fzf-vim
    coc-fzf
    coc-pyright
    coc-clangd
  ];
  extraConfig = ''
    " set leader to space
    let mapleader=" "

    " indentation (2 spaces)
    set tabstop=2
    set softtabstop=2
    set shiftwidth=2
    set expandtab

    " line numbering
    set relativenumber
    set number

    " wrap at 80 chars
    set tw=79
    set nowrap
    set fo-=t

    " more space for error messages
    set cmdheight=2

    " make snappy woop
    set updatetime=300

    " airline
    let g:airline_theme='base16'

    " ALE
    let g:ale_linters = {'rust': ['cargo']}
    nmap <silent> <C-k> <Plug>(ale_previous_wrap)
    nmap <silent> <C-j> <Plug>(ale_next_wrap)

    " syntastic
    let g:syntastic_rust_checkers = ['cargo']
    
    " show true colors
    " this messes up my pretty color scheme :'(
    " set termguicolors

    " set up easymotion
    map <Leader> <Plug>(easymotion-prefix)

    " symbol search w/ coc-fzf
    nnoremap <Leader><Leader>s  :<C-u>CocList -I symbols<cr>
    nnoremap <Leader><Leader>s  :<C-u>CocSearch <cr>
  '';
}
