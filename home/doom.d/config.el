(setq display-line-numbers-type 'relative)

;; set error highlighting in a terminal-renderable way
;; Disable default fringe styling
(setq +vc-gutter-default-style nil)
;; ...and make sure the margin is visible to begin with
(setq-default left-margin-width 2)

;; Move flycheck to left margin
(setq-default flycheck-indication-mode 'left-margin)

;; black - python autoformatting
(use-package! python-black
  :demand t
  :after python)
(add-hook! 'python-mode-hook #'python-black-on-save-mode)
(map! :leader :desc "Blacken Buffer" "m b b" #'python-black-buffer)
(map! :leader :desc "Blacken Region" "m b r" #'python-black-region)
(map! :leader :desc "Blacken Statement" "m b s" #'python-black-statement)

;; format Elm files on save
(add-hook 'elm-mode-hook 'elm-format-on-save-mode)
