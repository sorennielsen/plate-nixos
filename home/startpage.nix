{ startpage ? import ./spinner-startpage/default.nix {} }:

# this only sets the path of the exported html file
# I think one should be able to set this in the derivation somehow
# but I'm a beginner so w/e for now...
{
  url = "file://${startpage.outPath}/Spinner.html";
}
