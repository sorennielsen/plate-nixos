{ colors ? import ./colors.nix
, pkgs ? import <nixpkgs> {} }:

{
  enable = true;

  # font
  font = {
    # XXX: this does not work
    package = pkgs.terminus_font_ttf;
    name = "Terminus (TTF)";
  };

  settings = {
    font_size = 15;

    background = colors.background;
    foreground = colors.foreground;
    # selection_background = colors.color5;
    # selection_foreground = colors.color0;
    # url_color = colors.color4;
    # cursor = colors.color5;
    # active_border_color = colors.color3;
    # inactive_border_color = colors.color1;
    # active_tab_background = colors.color0;
    # active_tab_foreground = colors.color5;
    # inactive_tab_background = colors.color1;
    # inactive_tab_foreground = colors.color4;
    # tab_bar_background = colors.color1;

    # normal
    color0 = colors.color0;
    color1 = colors.color1;
    color2 = colors.color2;
    color3 = colors.color3;
    color4 = colors.color4;
    color5 = colors.color5;
    color6 = colors.color6;
    color7 = colors.color7;

    # bright
    color8 = colors.color8;
    color9 = colors.color9;
    color10 = colors.color10;
    color11 = colors.color11;
    color12 = colors.color12;
    color13 = colors.color13;
    color14 = colors.color14;
    color15 = colors.color15;

  };

  extraConfig = ''
    confirm_os_window_close 2
  '';
}
