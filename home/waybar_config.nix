{ colors ? import ./colors.nix }:

''
 {
   "layer": "bottom",
   "position": "top",
   "height": 30,
   "modules-left": ["sway/workspaces", "sway/mode", "custom/media"],
   "modules-center": ["sway/window"],
   "modules-right": ["idle_inhibitor", "backlight", "pulseaudio", "network", "cpu", "memory", "temperature", "battery", "battery#bat2", "clock", "tray"],
   // Modules configuration
   "sway/workspaces": {
     "disable-scroll": true,
     "all-outputs": false,
     "format": "{icon}",
     "format-icons": {
       "1": "",
       "2": "",
       "3": "",
       "4": "IV",
       "5": "V",
       "6": "VI",
       "7": "VII",
       "8": "VIII",
       "9": "IX",
       "10": "",
       "11": "XI",
       "12": "XII",
       "13": "XIII",
       "14": "XIV",
       "15": "XV",
       "16": "XVI",
       "17": "XXVII",
       "18": "XVIII",
       "19": "XIX",
       "20": "XX",
       "urgent": ""
       // "focused": "",
       // "default": ""
     }
   },
   "idle_inhibitor": {
     "format": "{icon}",
     "format-icons": {
       "activated": "",
       "deactivated": ""
     }
   },
   "clock": {
     // "timezone": "America/New_York",
     "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
     "format-alt": "{:%Y-%m-%d}"
   },
   "cpu": {
     "format": "  {usage}%",
     "tooltip": true
   },
   "memory": {
     "format": "  {used:.2} GiB"
   },
   "temperature": {
     // "thermal-zone": 2,
     // "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
     "critical-threshold": 80,
     // "format-critical": "{temperatureC}°C {icon}",
     "format": "{icon} {temperatureC}°C",
     "format-icons": ["", "", ""]
   },
   "backlight": {
     // "device": "acpi_video1",
     "format": "{percent}% {icon}",
     "format-icons": ["", ""]
   },
   "battery": {
     "states": {
       // "good": 95,
       "warning": 30,
       "critical": 15
     },
     "format": "{icon} {capacity}%",
     "format-charging": "{capacity}% ",
     "format-plugged": "{capacity}% ",
     "format-alt": "{time} {icon}",
     // "format-good": "", // An empty format will hide the module
     // "format-full": "",
     "format-icons": ["", "", "", "", ""]
   },
   "battery#bat2": {
     "bat": "BAT2"
   },
   "network": {
     // "interface": "wlp2*", // (Optional) To force the use of this interface
     "format-wifi": " {essid} ({signalStrength}%) ",
     "format-ethernet": " {ifname}: {ipaddr}/{cidr}",
     "format-linked": " {ifname} (No IP)",
     "format-disconnected": " Offline",
     "format-alt": "{ifname}: {ipaddr}/{cidr}"
   },
   "pulseaudio": {
     // "scroll-step": 1, // %, can be a float
     "format": "{icon} {volume}% {format_source}",
     "format-muted": " {format_source}",
     "format-source": "{volume}% ",
     "format-source-muted": "",
     "format-icons": {
       "headphone": "",
       "hands-free": "",
       "headset": "",
       "phone": "",
       "portable": "",
       "car": "",
       "default": ["", "", ""]
     },
     "on-click": "",
     "format-bluetooth": "{volume}% {icon} {format_source}",
     "format-bluetooth-muted": " {icon} {format_source}"
   },
   "custom/media": {
     "format": "{icon} {}",
     "return-type": "json",
     "max-length": 40,
     "format-icons": {
       "spotify": "",
       "default": "🎜"
     },
     "escape": true,
     "exec": "$HOME/.config/waybar/mediaplayer.py 2> /dev/null" // Script in resources folder
     // "exec": "$HOME/.config/waybar/mediaplayer.py --player spotify 2> /dev/null" // Filter player based on name
   }
 }
''
