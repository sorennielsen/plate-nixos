{ colors ? import ./colors.nix }:

{
  enable = true;
  shellInit = ''
    # this is WIP
    set -u fish_color_cancel "red"
    set -u fish_color_command "blue"
    set -u fish_color_comment "${colors.color9}"
    set -u fish_color_end "red"
    set -u fish_color_error "${colors.color9}"
    set -u fish_color_escape "green"
    set -u fish_color_normal "${colors.foreground}"
    set -u fish_color_operator "${colors.color8}"
    set -u fish_color_param "${colors.color7}"
    set -u fish_color_quote "${colors.color11}"
    set -u fish_color_redirection "${colors.foreground}"
    set -u fish_color_valid_path "blue"
    set -u fish_greeting ""

    # start starship
    starship init fish | source

    # set up direnv for fish
    eval (direnv hook fish)

    alias vim "emacsclient -c -nw"
    alias e "emacsclient -c -nw"
  '';
}
