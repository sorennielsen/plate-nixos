#  Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running ‘nixos-help’).
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      <home-manager/nixos>
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # kernel modules needed for audio stuff
  # (https://nixos.wiki/wiki/JACK)
  boot.kernelModules = [
    "snd-seq"      # sequencing
    "snd-rawmidi"  # midi
  ];

  # let inotify do a lot
  boot.kernel.sysctl = {
    # Note that inotify watches consume 1kB on 64-bit machines.
    "fs.inotify.max_user_watches"   = 1048576;   # default:  8192
    "fs.inotify.max_user_instances" =    1024;   # default:   128
    "fs.inotify.max_queued_events"  =   32768;   # default: 16384
  };

  # Networking
  networking = {
    hostName = "plate";
    networkmanager.enable = true;
    nameservers = ["91.239.100.100" "1.1.1.1"];

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    useDHCP = false;
    interfaces.wlp0s20f3.useDHCP = true;

    # vpn
    wireguard.interfaces.am = import ./private/wireguard/am.nix;
    # wireguard.interfaces.vpnbox = import ./private/wireguard/vpnbox.nix;
    wireguard.interfaces.skelby = import ./private/wireguard/skelby.nix;

    firewall = {
      enable = true;
      allowedTCPPorts = [
        1234  # used for random netcatting and stuff
        1337  # used for random netcatting and stuff
      ];
      allowedUDPPorts = [ 51820 ]; # for wireguard
    };

    extraHosts =
      ''
        127.0.0.1 plate
        10.10.10.111 obsidebian.wg
      '';
  };

  # internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
  time.timeZone = "Europe/Copenhagen";

  environment.variables = {
    EDITOR = "nvim";

    # needed by qutebrowser
    QT_QPA_PLATFORM = "wayland";
  };

  # allow non-free packages
  # SIGH
  nixpkgs.config.allowUnfree = true;

  # add Nix User Repository
  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
      inherit pkgs;
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs;
  [
    androidenv.androidPkgs_9_0.platform-tools  # adb
    ansible          # 🐮 cattle management
    ascii            # ascii tables
    asciinema        # terminal recording
    apacheHttpd      # useful for `htpasswd` command
    aseprite         # pixel art editor
    atop             # detailed resource monitor
    audacity         # audio schmaudio
    avahi            # mdns stuff
    bandwhich        # cli network monitor
    bemenu           # launcher
    bintools-unwrapped  # strings
    brightnessctl    # control display brightness
    brogue           # a good roguelike, bro.
    ungoogled-chromium  #  another browser
    clang            # a c compiler
    cloc             # count lines of code
    cointop          # shitcoin tracker
    cowsay           # moo!
    direnv           # cwd-based virtual environments
    discord          # some chat app [SIGH]
    dogdns           # 🐕 better dig
    docker           # 🐋 containers
    docker-compose   # 🐳 more containers
    element-desktop  # matrix client
    fd               # search file names
    ffmpeg           # media tool that can do anything
    ffsend           # upload to send.firefox.com
    feh              # simple image viewer
    file             # file type detection util
    firefox-wayland  # another browser
    fish             # a good shell
    gcc              # C compiler
    ghc              # Haskell compiler
    ghidra-bin       # 🐲 static analysis w/ a good decompiler
    git              # version control
    git-lfs          # 4 thicc files
    gimp             # 🫑 green is my pepper
    gnumake          # inferior build system
    gnome3.adwaita-icon-theme  # needed for lutris
    gnome3.nautilus  # gui file manager
    google-cloud-sdk # cli for cloud
    gotop            # like htop but prettier
    grim             # scrot for wayland
    htop             # performance monitoring
    home-manager     # manage dotfiles with nix
    httpie           # better than curl
    id3v2            # mp3 metadata
    inetutils        # telnet
    inkscape         # vector art
    ipfs             # 🪐INTERPLANETARY
    jq               # json utility
    keepassxc        # password manager
    kitty            # a terminal with proper emoji rendering
    libreoffice      # WYSIWYG document manipulation
    # lutris           # for games that only work on bad OSes
    minecraft        # yes
    mpv              # media playback
    mupdf            # pdf viewer
    ncdu             # ncurses disk usage
    ncmpcpp          # mpd client
    neofetch         # pretty system specs
    neovim           # a new editor
    networkmanager   # networking daemon
    nheko            # a neat-o matrix client 🐱
    nodejs           # ☕📜
    nicotine-plus    # soulseek client
    nixFlakes        # ❄ new deps management w/ nix
    nmap             # network scanning
    openvpn          # *sigh* a vpn. HTB and DTU use it
    p7zip            # archival tool
    pavucontrol      # gui audio stuff
    pamixer          # tui audio stuff
    pandoc           # document conversions
    pango            # font rendering
    pciutils         # lspci
    pipewire         # next gen multipedia routing
    pipewire-media-session  # session manager for above
    prettyping       # ping with colors
    pwndbg           # 💀 pwning w/ gdb
    python310        # 🐍 a good lang
    python310Packages.binwalk    # take binary mess part
    python310Packages.numpy      # fancy arrays
    python310Packages.pip        # python package manager
    python310Packages.pip-tools  # pin pip dependencies
    python310Packages.requests   # HTTP for Humans™
    python310Packages.black      # python code formatter
    python310Packages.mypy       # python type checking
    python39Packages.ipython     # ergonomic python repl
    python39Packages.numpy       # fancy arrays
    qbittorrent      # a good bittorrent client
    qutebrowser      # a good browser
    qt5.qtwayland    # wayland plugin for QT
    ranger           # tui file manager
    remmina          # an RDP client that Just Works™
    ripgrep          # search file content
    sd               # a better sed
    sl               # choo choo!
    solo2-cli        # cli for the solokey v2
    signal-desktop   # some people just haven't seen the light yet
    speedtest-cli    # cli network tester
    sshuttle         # ssh tunnels on steroids 🦾
    starship         # a comfy cli prompt
    steam            # 🎮 vidya
    superTuxKart     # 🐧🏎
    sway             # window manager
    sway-contrib.grimshot  # better screenshots in wayland
    syncthing        # file sync
    tabnine          # autocomplete
    texlive.combined.scheme-full  # LaTeX
    thunderbird-bin-91  # the least bad email client? maybe?
    tmux             # terminal multiplexing
    toilet           # ascii art text
    # tor-browser-bundle-bin  # 🧅🌐 <- borked
    tabnine          # ML-powered code completion
    vimPlugins.completion-tabnine
    traceroute       # tracing ip packets
    tree             # pretty directory view
    units            # unit conversions
    usbutils         # lsusb
    virtualbox       # virtual machines
    viu              # terminal feh
    vscode           # microsoft <3 linux
    xdg_utils        # some desktop programs need xdg_open
    websocat         # socat but websockets
    wireguard-tools  # a good vpn
    wireshark        # packet captures
    wofi             # new launcher
    wl-clipboard     # wayland clipboard cli
    youtube-dl       # a good multimedia downloader
    zathura          # a powerful pdf viewer
    zoxide           # better cd
  ];
  # ] ++ [ (import ./emocli/default.nix) ];

  # nixos wiki says to set sound.enable=false when using pipewire
  sound.enable = false;

  # pipewire shit
  # this is recommended installed, but seems to trigger a *huge* compile
  # security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  # faster nix-shell builds
  services.lorri.enable = true;

  # docker
  virtualisation.docker.enable = true;

  # virtualbox
  virtualisation.virtualbox.host.enable = true;
  virtualisation.virtualbox.guest.enable = true;
  nixpkgs.config.virtualbox.host.enableExtensionPack = true;
  users.extraGroups.vboxusers.members = [ "asbjorn" ];

  # fonts
  fonts.fonts = with pkgs; [
    # emojione           # an emoji font
    # joypixels          # updated emojione?
    # noto-fonts-emoji
    aileron            # YEAH HELVETICA
    font-awesome       # some icons
    inconsolata        # decent font
    terminus_font      # better font (that I can't get to work in sway)
    terminus_font_ttf  # better font (that I can't get to work in sway)
    eb-garamond
  ];

  # sway window manager
  programs.sway = {
    enable = true;
    extraPackages = with pkgs; [
      swaylock
      kanshi
      swayidle
      waybar
      xwayland
    ];
  };

  # yep
  programs.steam.enable = true;

  # fix wireshark permissions
  programs.wireshark.enable = true;

  # user-specific stuff below here
  # users
  users.users.asbjorn = {
    isNormalUser = true;
    extraGroups = [
      "audio"
      "docker"
      "wheel"
      "wireshark"
    ];
  };

  services = {
    # syncthing
    syncthing = {
      enable = true;
      user = "asbjorn";
      dataDir = "/home/asbjorn/Sync";  # default data dir - not actually used
      configDir = "/home/asbjorn/.config/syncthing";
    };

    # printing
    printing.enable = true;
    printing.drivers = [
      pkgs.gutenprint
      pkgs.gutenprintBin
      pkgs.hplip
    ];

    # this helps against pip-compile running out of space on /run/user/${UID}
    # by allowing systemd logind to allocate up to 50% of memory to the user runtime dir
    # /shrug idk what this actually is
    logind.extraConfig = ''
      RuntimeDirectorySize=50%
    '';
  };

  # fish shell
  programs.fish.enable = true;
  users.users.asbjorn = {
    shell = pkgs.fish;
  };

  # dotfile stuff with home-manager
  home-manager.users.asbjorn = { pkgs, ... }: {
    home.stateVersion = "22.05";

    # symlink ~/Downloads/ to /tmp/
    home.file."Downloads".source = pkgs.runCommand "symlink-tmp" {} ''
      ln -s /tmp/ $out
    '';

    # fish
    programs.fish = import ./home/fish.nix {};

    # git
    programs.git = {
      enable = true;
      userName = "AsbjornOlling";
      userEmail = "asbjornolling@gmail.com";
      lfs.enable = true;
    };

    # sway
    wayland.windowManager.sway = import ./home/sway.nix {};

    xdg = {
      # waybar (statusbar)
      configFile."waybar/config".text = import ./home/waybar_config.nix {};
      configFile."waybar/style.css".text = import ./home/waybar_style.nix {};

      # wofi (launcher)
      configFile."wofi/style.css".text = import ./home/wofi_style.nix {};

      # pscircle: process tree wallpapers
      configFile."pscircle/run.sh".text = import ./home/pscircle.nix { sway = pkgs.sway; };
    };

    # termite
    programs.termite = import ./home/termite.nix {};

    # kitty
    programs.kitty = import ./home/kitty.nix {};

    # neovim
    programs.neovim = import ./home/neovim.nix {};

    # qutebrowser
    programs.qutebrowser = import ./home/qutebrowser.nix {};

    # doom emacs
    home.packages = [ ((import ./home/emacs.nix) {}).doom-emacs ];

    # start emacs daemon (so emacs can start fast)
    services.emacs = {
      enable = true;
      package = ((import ./home/emacs.nix) {}).doom-emacs;
    };

  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
